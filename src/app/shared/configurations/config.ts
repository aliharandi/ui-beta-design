export function isArray(object: any): boolean {
    return typeof object === 'object' && !!object.length && !isNaN(object.length);
}
export function randomNumber(): number {
    return Math.floor(Math.random() * 10000000);
}
