import {Injectable} from '@angular/core';
import {Panel} from '../../layout-renderer/shared/panel.model';
import {MenuItem} from '../menu-builder/menu-item.model';

@Injectable({providedIn: 'root'})
export class Constants {
    public readonly TOP_DISTANT_OFFSET = 95;
    public readonly WINDOW_WIDTH_OFFSET = 10;
    public readonly TYPE_IN_PAGE: 'In-Page' | 'Pop-Up' = 'In-Page';
    public readonly TYPE_POPUP: 'In-Page' | 'Pop-Up' = 'Pop-Up';
    public PANELS: Panel[] = [];
    public SAVE_ACTION = 'SAVE';
    public CANCEL_ACTION = 'CANCEL';
    public PUBLISH_ACTION = 'PUBLISH';
    public APPLY_ACTION = 'APPLY';
    public OPEN_ACTION = 'OPEN';
    public CLOSE_ACTION = 'CLOSE';
    public SELECT_ACTION = 'SELECT';
    public CREATE_ACTION = 'CREATE';
    public UPDATE_ACTION = 'UPDATE';
    public DELETE_ACTION = 'DELETE';
    public configPanelSize = 300;
    public listPanelSize = 700;
    public menu: MenuItem[] = [
        {id: 1, name: 'dashboards', label: 'Dashboards', route: '', isActive: false},
        {id: 2, name: 'workspace', label: 'Workspace', route: '', isActive: false},
        {id: 3, name: 'exploration', label: 'Exploration', route: '', isActive: false},
        {id: 3, name: 'executive', label: 'Executive', route: '', isActive: false, relatedMenuItemId: 1},
        {id: 4, name: 'monitoring', label: 'Monitoring', route: '', isActive: false},
        {id: 5, name: 'operational', label: 'Operational', route: '', isActive: false},
        {id: 6, name: 'admin', label: 'Admin', route: '', isActive: false},
        {id: 7, name: 'userManagement', label: 'User Management', route: '', isActive: false, relatedMenuItemId: 6},
        {id: 8, name: 'moduleManagement', label: 'Module Management', route: '', isActive: false, relatedMenuItemId: 6},
        {id: 9, name: 'listManagement', label: 'List Management', route: '', isActive: false, relatedMenuItemId: 6}
    ];
}
