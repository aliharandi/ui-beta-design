import {MenuItem} from './menu-item.model';

export class Menu {
    public activePrimaryItemId: number;
    public activeSecondaryItemId: number;
    public items: PrimaryMenuItem[];

    constructor(menuItems: MenuItem[]) {
        this.items = this.generateMenu(menuItems);
    }

    public generateMenu(menuItems: MenuItem[]): PrimaryMenuItem[] {
        const primaryItems: PrimaryMenuItem[] = [];
        menuItems.forEach(menuItem => {
            if (!menuItem.relatedMenuItemId) {
                primaryItems.push(new PrimaryMenuItem(menuItem.id, menuItem.name, menuItem.label, menuItem.route, menuItem.isActive,
                    menuItem.relatedMenuItemId, menuItem.alternateLabel, []));
            }
        });
        const secondaryItems = menuItems.filter(menuItem => !!menuItem.relatedMenuItemId);
        secondaryItems.forEach(menuItem => {
            const foundPrimaryItem = primaryItems.find(primaryItem => primaryItem.id === menuItem.relatedMenuItemId);
            if (!!foundPrimaryItem) {
                foundPrimaryItem.secondaryItems.push(menuItem);
            }
        });
        return primaryItems;
    }

    public activateItem(primaryItemId: number, secondaryItemId: number, toggle = true): void {
        const primaryItem = this.items.find(item => item.id === primaryItemId);
        if (!!primaryItem) {
            const secondaryItem = primaryItem.secondaryItems.find(item => item.id === secondaryItemId);
            if (!!secondaryItem) {
                secondaryItem.isActive = toggle;
                if (toggle && !!this.activePrimaryItemId && !!this.activeSecondaryItemId) {
                    this.activateItem(this.activePrimaryItemId, this.activeSecondaryItemId, false);
                }
                if (toggle) {
                    this.activePrimaryItemId = primaryItemId;
                    this.activeSecondaryItemId = secondaryItemId;
                }
            }
        }
    }
    public findSelectedMenuLabel(primaryItemId: number, secondaryItemId: number): string {
        const primaryItem = this.items.find(item => item.id === primaryItemId);
        if (!!primaryItem) {
            const secondaryItem = primaryItem.secondaryItems.find(item => item.id === secondaryItemId);
            if (!!secondaryItem) {
                return secondaryItem.label;
            }
        }
    }
}

export class PrimaryMenuItem extends MenuItem {

    public secondaryItems: MenuItem[];
    public onHover = false;

    constructor(id: number, name: string, label: string, route: string, isActive?: boolean, relatedMenuItemId?: number,
                alternateLabel?: string, secondaryItems?: MenuItem[]) {
        super(id, name, label, route, isActive, relatedMenuItemId, alternateLabel);
        if (!!secondaryItems) {
            this.secondaryItems = secondaryItems;
        } else {
            this.secondaryItems = [];
        }
    }
}
