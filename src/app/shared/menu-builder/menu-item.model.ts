export class MenuItem {
    public readonly id: number;
    public readonly name: string;
    public readonly label: string;
    public readonly route: string;
    public isActive = false;
    public readonly relatedMenuItemId?: number;
    public readonly alternateLabel?: string;

    constructor(id: number, name: string, label: string, route: string, isActive?: boolean, relatedMenuItemId?: number,
                alternateLabel?: string) {
        this.id = id;
        this.name = name;
        this.label = label;
        this.route = route;
        if (!!isActive) {
            this.isActive = isActive;
        } else {
            this.isActive = false;
        }
        if (!!relatedMenuItemId) {
            this.relatedMenuItemId = relatedMenuItemId;
        } else {
            this.relatedMenuItemId = null;
        }
        if (!!alternateLabel) {
            this.alternateLabel = alternateLabel;
        } else {
            this.alternateLabel = null;
        }
    }
}
