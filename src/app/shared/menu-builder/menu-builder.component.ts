import {Component, OnInit} from '@angular/core';
import {Constants} from '../configurations/constants';
import {MenuItem} from './menu-item.model';
import {Menu} from './menu.model';

@Component({
    selector: 'app-menu-builder',
    templateUrl: './menu-builder.component.html',
    styleUrls: ['./menu-builder.component.scss']
})
export class MenuBuilderComponent implements OnInit {

    private readonly menuItems: MenuItem[];
    public menu: Menu;

    constructor(private constants: Constants) {
        this.menuItems = this.constants.menu;
    }

    ngOnInit(): void {
        this.menu = new Menu(this.menuItems);
    }

}
