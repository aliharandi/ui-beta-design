import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

import {Constants} from '../configurations/constants';
import {FormGroup} from '@angular/forms';
import {ViewDimension} from '../../layout-renderer/shared/layout.model';

@Injectable()
export class RootService {

    public constant: Constants;
    public selectedList$: Observable<any>;
    public form$: Observable<FormGroup>;
    public viewDimensions$: Observable<ViewDimension>;
    private viewDimensionsSubject$ = new BehaviorSubject<ViewDimension>(undefined);

    constructor(constant: Constants) {
        this.constant = constant;
        this.viewDimensions$ = this.viewDimensionsSubject$.asObservable();
    }

    setViewDimensionsSubjectValue(dimensions: ViewDimension): void {
        this.viewDimensionsSubject$.next(dimensions);
    }
}
