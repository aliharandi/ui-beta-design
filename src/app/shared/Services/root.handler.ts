import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';

import {Constants} from '../configurations/constants';
import {EntityCollectionServiceBase, QueryParams} from '@ngrx/data';

@Injectable()
export class RootHandler {

    constant: Constants;
    entityService: EntityCollectionServiceBase<any>;

    constructor(private injector: Injector, constant: Constants, entityService: EntityCollectionServiceBase<any>) {
        this.constant = constant;
        this.entityService = entityService;
    }

    getSummarised(filter?: QueryParams, customUrl?: string): Observable<any> {
        return this.entityService.getWithQuery(filter as QueryParams);
    }

    getDetails(filter?: QueryParams, customUrl?: string): Observable<any> {
        return this.entityService.getWithQuery(filter as QueryParams);
    }

    getDetailsById(filter?: QueryParams, customUrl?: string): Observable<any> {
        return this.entityService.getWithQuery(filter as QueryParams);
    }

    update(object: any): Observable<any> {
        return this.entityService.update(object);
    }

    add(object: any): Observable<any> {
        return this.entityService.add(object);
    }

    delete(id: number | string): Observable<any> {
        return this.entityService.delete(id);
    }

    publish(object: any): Observable<any> {
        return this.entityService.update(object);
    }

    apply(object: any): Observable<any> {
        return this.entityService.update(object);
    }
}
