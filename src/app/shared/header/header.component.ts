import {Component, OnInit, ViewChild} from '@angular/core';
import {ButtonComponent} from '@syncfusion/ej2-angular-buttons';
import {SidebarComponent} from '@syncfusion/ej2-angular-navigations';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    @ViewChild('sidebar') sidebar: SidebarComponent;
    public target = '.content';
    @ViewChild('SidebarButton')
    public toggleButton: ButtonComponent;

    constructor() {
    }

    ngOnInit(): void {
    }

    public onCreated(args: any): void {
        this.sidebar.element.style.visibility = '';
    }

    public sidebarCloseButtonClick(): void {
        this.sidebar.hide();
        this.toggleButton.element.classList.remove('e-active');
    }

    public sidebarButtonClick(): void {
        if (this.toggleButton.element.classList.contains('e-active')) {
            this.sidebar.hide();
        } else {
            this.sidebar.show();
        }
    }
}
