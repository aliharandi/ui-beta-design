import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {ButtonModule} from '@syncfusion/ej2-angular-buttons';
import {SidebarModule} from '@syncfusion/ej2-angular-navigations';
import {MenuBuilderComponent} from './menu-builder/menu-builder.component';
import {MatButtonModule} from '@angular/material/button';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
    declarations: [
        HeaderComponent,
        MenuBuilderComponent
    ],
    exports: [
        HeaderComponent
    ],
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        SidebarModule,
        ButtonModule,
        MatButtonModule
    ]
})
export class SharedModule {
}
