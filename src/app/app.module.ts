import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {SplitterModule} from '@syncfusion/ej2-angular-layouts';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {LayoutRendererComponent} from './layout-renderer/layout-renderer.component';
import {LayoutViewComponent} from './layout-renderer/layout-view/layout-view.component';
import {LayoutPanelComponent} from './layout-renderer/layout-panel/layout-panel.component';
import {LayoutListViewComponent} from './layout-renderer/layout-list-view/layout-list-view.component';
import {LayoutFormViewComponent} from './layout-renderer/layout-form-view/layout-form-view.component';
import {LayoutDialogViewComponent} from './layout-renderer/layout-dialog-view/layout-dialog-view.component';
import {SharedModule} from './shared/shared.module';

@NgModule({
    declarations: [
        AppComponent,
        LayoutRendererComponent,
        LayoutViewComponent,
        LayoutPanelComponent,
        LayoutListViewComponent,
        LayoutFormViewComponent,
        LayoutDialogViewComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        SplitterModule,
        PerfectScrollbarModule,
        SharedModule,
        BrowserAnimationsModule
    ],
    providers: [
      // { provide: LOCALE_ID, useValue: 'fr' }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
