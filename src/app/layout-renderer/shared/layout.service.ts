import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {cloneDeep} from 'lodash';

import {Panel} from './panel.model';
import {ViewAndPanelsCurrentState, ViewDimension} from './layout.model';

@Injectable()
export class LayoutService {
    protected panelSubject$ = new BehaviorSubject<Panel[]>([]);
    public readonly panels$: Observable<Panel[]>;
    protected scrollSubjects$ = new Subject<{ x: number, y: number }>();
    public readonly scroll$: Observable<{ x: number, y: number }>;
    protected panels: Panel[] = [];
    protected viewAndPanelsCurrentState: ViewAndPanelsCurrentState;
    private viewDimension: ViewDimension;


    constructor() {
        this.panels$ = this.panelSubject$.asObservable();
        this.scroll$ = this.scrollSubjects$.asObservable();
    }

    setPanels(panels: Panel[]): void {
        const hasPrimaryPanel = !!panels.find(panel => panel.isPrimary = true);
        const panelsHavePriority = !panels.find(panel => isNaN(panel.priority));
        if (hasPrimaryPanel) {
            panels.forEach((panel, index) => {
                const panelIndex = this.panels.findIndex(layoutPanel => layoutPanel.id === panel.id);
                if (!!panel.id && panelIndex === -1) {
                    if (!panelsHavePriority) {
                        panel.priority = index;
                    }
                    this.panels.push(panel);
                } else {
                    // throw error(`Can't Setup Panels, this Panel: ` + panel + ' has no ID');
                }
            });
            this.panels.sort((panelA, panelB) => panelA.priority - panelB.priority);
        } else {
            // throw error(`Can't Setup Panels, There is no Primary Panel has been defined`);
        }
        if (this.panels.length > 0) {
            this.panelSubject$.next(cloneDeep(this.panels));
        } else {
            // throw error(`Can't Setup Panels, No Panels has been defined.`);
        }
    }

    showPanel(panelId: number): void {
        if (!!panelId && typeof panelId === 'number') {
            if (!!this.panels.find(panel => panel.id === panelId)) {
                (this.panels.find(panel => panel.id === panelId)).isActive = true;
                this.panelSubject$.next(cloneDeep(this.panels));
            } else {
                // throw error('*ON SHOW PANEL* There is no panel with this Id: ' + panelId);
            }
        } else {
            // throw error('*ON SHOW PANEL* Panel Id is not Recognizable');
        }
    }

    hidePanel(panelId: number): void {
        if (!!panelId && typeof panelId === 'number') {
            if (!!this.panels.find(panel => panel.id === panelId)) {
                (this.panels.find(panel => panel.id === panelId)).isActive = false;
                this.panelSubject$.next(cloneDeep(this.panels));
            } else {
                // throw error('*ON HIDE PANEL* There is no panel with this Id: ' + panelId);
            }
        } else {
            // throw error('*ON HIDE PANEL* Panel Id is not Recognizable');
        }
    }

    findPanel(id: number): Panel {
        if (!!this.panels.find(panel => panel.id === id)) {
            return this.panels.find(panel => panel.id === id);
        } else {
            // throw error('*ON FIND PANEL* There is no panel with this Id: ' + id);
        }
    }

    setViewDimensions(dimensions: ViewDimension): void {
        if (!!dimensions.windowWidth) {
            this.viewDimension.windowWidth = dimensions.windowWidth;
        }
        if (!!dimensions.windowHeight) {
            this.viewDimension.windowHeight = dimensions.windowHeight;
        }
        if (!!dimensions.topViewHeight) {
            this.viewDimension.topViewHeight = dimensions.topViewHeight;
        }
        if (!!dimensions.mainViewHeight) {
            this.viewDimension.mainViewHeight = dimensions.mainViewHeight;
        }
        if (!!dimensions.mainViewWidth) {
            this.viewDimension.mainViewWidth = dimensions.mainViewWidth;
        }
    }

    getViewDimensions(): ViewDimension {
        return cloneDeep(this.viewDimension);
    }

    scrollToPosition(x?: number, y?: number): void {
        this.scrollSubjects$.next({x: x ? x : 0, y: y ? y : 0});
    }

    getPanelIdByName(panelName: string): number {
        if (!!this.panels.find(panel => panel.name === panelName)) {
            return this.panels.find(panel => panel.name === panelName).id;
        } else {
            // throw error('*ON FIND PANEL ID* There is no panel with this name: ' + panelName);
        }
    }

    setViewAndPanelsCurrentState(mainContainerWidth: number, mainContainerHeight: number, splitterWidth: number,
                                 Panels: Panel[]): void {
        this.viewAndPanelsCurrentState.mainContainerHeight = mainContainerHeight;
        this.viewAndPanelsCurrentState.mainContainerWidth = mainContainerWidth;
        this.viewAndPanelsCurrentState.splitterWidth = splitterWidth;
        this.viewAndPanelsCurrentState.Panels = Panels;
    }

    getViewAndPanelsCurrentState(): ViewAndPanelsCurrentState {
        return cloneDeep(this.viewAndPanelsCurrentState);
    }
}
