import {TemplateRef} from '@angular/core';

export class Tab {
    constructor(
        public name: string,
        public isActive: boolean,
        public view: TemplateRef<any>,
        public CSSClass?: string
    ) {
    }
}


