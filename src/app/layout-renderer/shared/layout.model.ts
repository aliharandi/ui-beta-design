import {Panel} from './panel.model';

export interface WindowSize {
    width: number;
    height: number;
}

export interface ViewDimension {
    windowWidth?: number;
    windowHeight?: number;
    topViewHeight?: number;
    mainViewWidth?: number;
    mainViewHeight?: number;
}

export class LayoutBuilderEventArgs {
    constructor(
        public panelId: number,
        public action: 'SAVE' | 'CANCEL' | 'PUBLISH' | 'APPLY' | 'OPEN' | 'CLOSE' | 'SELECT' | 'CREATE' |
            'UPDATE' | 'DELETE',
        public payload?: any,
        public options?: {}
    ) {
    }
}

export interface ViewAndPanelsCurrentState {
    mainContainerWidth: number,
    mainContainerHeight: number,
    splitterWidth: number,
    Panels: Panel[],
}
