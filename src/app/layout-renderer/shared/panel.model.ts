import {TemplateRef} from '@angular/core';
import {cloneDeep} from 'lodash';

import {Tab} from './tab.model';

export class Panel {
    protected _ID: number;
    public name: string;
    public tabs: Tab[];
    public content: TemplateRef<any>;
    public isPrimary: boolean;
    public minWidth: number;
    public isActive: boolean;
    public type?: 'In-Page' | 'Pop-Up' = 'In-Page';
    public priority?: number;
    public resizable?: boolean;
    public size?: { width?: number, height?: number};
    public hasRelationPanel?: PanelRelation;
    public isCustomize?: boolean;

    constructor(
        name?: string,
        tabs?: Tab[],
        content?: TemplateRef<any>,
        isPrimary?: boolean,
        minWidth?: number,
        isActive?: boolean,
        type?: 'In-Page' | 'Pop-Up',
        priority?: number,
        resizable?: boolean,
        size?: { width?: number, height?: number},
        hasRelationPanel?: PanelRelation,
        isCustomize?: boolean
    ) {
        this.name = name;
        this.tabs = tabs;
        this.content = content;
        this.isPrimary = isPrimary;
        this.minWidth = minWidth;
        this.isActive = isActive;
        if (type) {
            this.type = type;
        }
        this.priority = priority;
        this.resizable = resizable;
        this.size = size;
        this.hasRelationPanel = hasRelationPanel;
        this.isCustomize = isCustomize;
    }

    set id(id: number) {
        this._ID = id;
    }

    get id(): number {
        return cloneDeep(this._ID);
    }
}

export class PanelRelation {
    constructor(
        public relatedPanelId: number,
        public canHide: boolean
    ) {
    }
}




