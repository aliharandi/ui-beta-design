import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {PerfectScrollbarComponent} from 'ngx-perfect-scrollbar';
import {fromEvent, Observable, Subscription} from 'rxjs';
import {ViewDimension, WindowSize} from '../shared/layout.model';
import {Constants} from '../../shared/configurations/constants';
import {LayoutService} from '../shared/layout.service';
import {debounceTime, map} from 'rxjs/operators';
import {RootService} from '../../shared/Services/root.service';

@Component({
    selector: 'app-layout-view',
    templateUrl: './layout-view.component.html',
    styleUrls: ['./layout-view.component.scss']
})
export class LayoutViewComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() topViewComponent: TemplateRef<any>;
    @ViewChild('TopViewComponent', {read: ElementRef}) topViewElementRef: ElementRef;
    @ViewChild(PerfectScrollbarComponent, {static: false}) scrollBar: PerfectScrollbarComponent;

    private windowResize$: Observable<WindowSize>;
    private windowResizeSubscription: Subscription;
    private scrollPositionSubscription: Subscription;
    windowHeightWithoutHeader: number;
    mainContainerWidth: number;
    mainContainerHeight: number;
    splitterWidth: number;
    private canScroll = false;

    constructor(public constant: Constants,
                private cdr: ChangeDetectorRef,
                public layoutService: LayoutService,
                public rootService: RootService
    ) {
        this.windowResize$ = fromEvent(window, 'resize').pipe(
            map((response: any) => {
                return {width: response.target.innerWidth, height: response.target.innerHeight};
            }),
            debounceTime(1000)
        );
    }

    ngOnInit(): void {
        this.windowResizeSubscription = this.windowResize$.subscribe(
            response => {
                if (!!response) {
                    this.mainContainerWidth = response.width - this.constant.WINDOW_WIDTH_OFFSET;
                    this.mainContainerHeight = response.height - this.constant.TOP_DISTANT_OFFSET -
                        this.topViewElementRef.nativeElement.offsetHeight;
                    const dimensions = this.layoutService.getViewDimensions();
                    dimensions.mainViewWidth = this.mainContainerWidth;
                    dimensions.mainViewHeight = this.mainContainerHeight;
                    this.rootService.setViewDimensionsSubjectValue(dimensions);
                    this.cdr.detectChanges();
                }
            }
        );
    }

    ngAfterViewInit(): void {
        this.windowHeightWithoutHeader = window.innerHeight - this.constant.TOP_DISTANT_OFFSET;
        this.mainContainerWidth = window.innerWidth - this.constant.WINDOW_WIDTH_OFFSET;
        this.splitterWidth = this.mainContainerWidth;
        this.mainContainerHeight = window.innerHeight - this.constant.TOP_DISTANT_OFFSET -
            this.topViewElementRef.nativeElement.offsetHeight;
        const dimensions: ViewDimension = {
            windowWidth: window.innerWidth,
            windowHeight: window.innerHeight,
            topViewHeight: this.topViewElementRef.nativeElement.offsetHeight,
            mainViewWidth: this.mainContainerWidth,
            mainViewHeight: this.mainContainerHeight
        };
        this.layoutService.setViewDimensions(dimensions);
        this.rootService.setViewDimensionsSubjectValue(this.layoutService.getViewDimensions());
        this.layoutService.panels$.pipe(
            map(
                response => {
                    response.forEach(panel => {
                        if ((panel.isPrimary || panel.isActive) && panel.type === this.constant.TYPE_IN_PAGE) {
                            if (!!panel.hasRelationPanel && !!panel.hasRelationPanel.relatedPanelId) {
                                const relatedPanelIndex = response
                                    .findIndex(activePanel => activePanel.id === panel.hasRelationPanel.relatedPanelId);
                                if (response[relatedPanelIndex].isActive) {
                                    response[relatedPanelIndex].size.width = response[relatedPanelIndex].size.width -
                                        panel.size.width;
                                }
                            }
                        }
                    });
                    let splitterWidthSize = 0;
                    response.forEach(panel => {
                        if ((panel.isPrimary || panel.isActive) && panel.type === this.constant.TYPE_IN_PAGE) {
                            splitterWidthSize = +panel.size.width;
                        }
                    });
                    if (splitterWidthSize <= this.mainContainerWidth) {
                        this.splitterWidth = this.mainContainerWidth;
                    } else {
                        this.canScroll = true;
                        this.splitterWidth = splitterWidthSize;
                    }
                    this.layoutService.setViewAndPanelsCurrentState(this.mainContainerWidth, this.mainContainerHeight,
                        this.splitterWidth, response);
                    return response;
                }
            )
        );
        this.scrollPositionSubscription = this.layoutService.scroll$.subscribe(
            response => {
                if (response && this.canScroll) {
                    this.scrollBar.directiveRef.scrollTo(response.x, response.y, 500);
                }
            }
        );
        this.cdr.detectChanges();
    }

    ngOnDestroy(): void {
        this.windowResizeSubscription.unsubscribe();
        this.scrollPositionSubscription.unsubscribe();
    }
}
