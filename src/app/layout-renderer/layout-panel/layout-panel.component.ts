import {AfterViewChecked, Component, Input, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ViewDimension} from '../shared/layout.model';
import {Constants} from '../../shared/configurations/constants';
import {LayoutService} from '../shared/layout.service';
import {Panel} from '../shared/panel.model';

@Component({
    selector: 'app-layout-panel',
    templateUrl: './layout-panel.component.html',
    styleUrls: ['./layout-panel.component.scss']
})
export class LayoutPanelComponent extends Panel implements OnInit, AfterViewChecked {
    @Input() private readonly identifier: number;
    @Input() private readonly viewDimensions: ViewDimension;
    @ViewChild('InPageCustomize', {read: ViewContainerRef}) inPageCustomizeRef: ViewContainerRef;
    @ViewChild('InPage', {read: ViewContainerRef}) inPageRef: ViewContainerRef;
    @ViewChild('PopUpCustomize', {read: ViewContainerRef}) popUpCustomizeRef: ViewContainerRef;
    @ViewChild('PopUp', {read: ViewContainerRef}) popUpRef: ViewContainerRef;

    isViewContainerRefInitiated = false;

    constructor(public constants: Constants, private layOutService: LayoutService) {
        super();
        const panel = this.layOutService.findPanel(this.identifier);
        this.name = panel.name;
        this.tabs = panel.tabs;
        this.content = panel.content;
        this.isPrimary = panel.isPrimary;
        this.minWidth = panel.minWidth;
        this.isActive = panel.isActive;
        this.type = panel.type;
        this.priority = panel.priority;
        this.resizable = panel.resizable;
        this.size = panel.size;
        this.hasRelationPanel = panel.hasRelationPanel;
        this.isCustomize = panel.isCustomize;
    }

    ngOnInit(): void {
    }

    ngAfterViewChecked(): void {
        if ((!!this.inPageCustomizeRef || !!this.inPageRef || !!this.popUpCustomizeRef || !!this.popUpRef)
            && !this.isViewContainerRefInitiated) {
            switch (this.type) {
                case 'In-Page': {
                    if (this.isCustomize) {
                        this.inPageCustomizeRef.createEmbeddedView(this.content);
                        this.isViewContainerRefInitiated = true;
                        break;
                    } else {
                        this.inPageRef.createEmbeddedView(this.content);
                        this.isViewContainerRefInitiated = true;
                        break;
                    }
                }
                case 'Pop-Up': {
                    if (this.isCustomize) {
                        this.popUpCustomizeRef.createEmbeddedView(this.content);
                        this.isViewContainerRefInitiated = true;
                        break;
                    } else {
                        this.popUpRef.createEmbeddedView(this.content);
                        this.isViewContainerRefInitiated = true;
                        break;
                    }
                }
            }
        }
    }
}
