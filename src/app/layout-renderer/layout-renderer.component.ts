import {Component, OnInit} from '@angular/core';
import {Panel} from './shared/panel.model';
import {Constants} from '../shared/configurations/constants';
import {LayoutService} from './shared/layout.service';

@Component({
    selector: 'app-layout-renderer',
    templateUrl: './layout-renderer.component.html',
    styleUrls: ['./layout-renderer.component.scss']
})
export class LayoutRendererComponent implements OnInit {

    constructor(private layoutService: LayoutService,
                private constants: Constants) {
    }

    ngOnInit(): void {
    }

    setPanels(Panels: Panel[]): void {
        this.layoutService.setPanels(this.constants.PANELS);
    }

    onOpenPanel(panelId: number): void {
        this.layoutService.showPanel(panelId);
        setTimeout(() => {
            const viewAndPanelsCurrentState = this.layoutService.getViewAndPanelsCurrentState();
            const activePanels = viewAndPanelsCurrentState.Panels
                .filter(panel => (panel.isPrimary || panel.isActive) && panel.type === this.constants.TYPE_IN_PAGE);
            if (viewAndPanelsCurrentState.splitterWidth > viewAndPanelsCurrentState.mainContainerWidth) {
                let x = 0;
                activePanels.forEach(panel => {
                    if (panel.id === panel.id && panel.hasRelationPanel && panel.hasRelationPanel.relatedPanelId) {
                        const relatedPanelWidth = (viewAndPanelsCurrentState.Panels
                            .find(relatedPanel => relatedPanel.id === panel.hasRelationPanel.relatedPanelId)).size.width;
                        this.layoutService.scrollToPosition(x - relatedPanelWidth);
                    } else {
                        this.layoutService.scrollToPosition(x);
                    }
                    x = +panel.size.width;
                });
            }
        }, 1);
    }

    getPanelIdByName(panelName: string): number {
        return this.layoutService.getPanelIdByName(panelName);
    }
}
